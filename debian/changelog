sqitch (1.5.0-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.5.0.
  * Update years of upstream and packaging copyright.
  * Update runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Feb 2025 01:15:11 +0100

sqitch (1.4.1-2) unstable; urgency=medium

  * Team upload.
  * autopkgtests: skip t/sqlite.t which is flaky on ci.debian.net.
    (Closes: #1085475)
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Oct 2024 01:36:08 +0200

sqitch (1.4.1-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.4.1.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 Feb 2024 00:48:25 +0100

sqitch (1.4.0-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.4.0.
  * Drop 2 patches (taken from upstream or fixed differently).
  * Update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 09 Aug 2023 02:59:53 +0200

sqitch (1.3.1-3) unstable; urgency=medium

  * Team upload.
  * Fix referenced package name in previous changelog entry.
  * Add a patch to (again) suppress warnings about 'Smartmatch'.
    Thanks to Niko Tyni for the bug report. (Closes: #1040532)

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Jul 2023 15:15:28 +0200

sqitch (1.3.1-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Add missing build dependency on dh-strip-nondeterminism for command
    dh_strip_nondeterminism.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.2, no changes needed.

  [ gregor herrmann ]
  * Add patch from upstream Git repo to fix tests with
    newer liburi-db-perl and liburi-perl.
  * Update years of packaging copyright.
  * Add test dependency on tzdata.
    Needed by (at least) t/plan_cmd.t.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Jun 2023 19:25:25 +0200

sqitch (1.3.1-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.3.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Oct 2022 21:19:26 +0200

sqitch (1.3.0-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.3.0.
  * Update years of upstream and packaging copyright.
  * Update test and runtime dependencies.
  * Declare compliance with Debian Policy 4.6.1.
  * Add libdbd-odbc-perl as an alternative runtime dependency
    used by some databases.
  * Add libdbd-sqlite3-perl and sqlite3 test dependencies
    to enable more tests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Aug 2022 15:05:31 +0200

sqitch (1.2.1-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * Build-Depends-Indep: Drop versioned constraint on libhash-merge-perl,
      libio-pager-perl, libtest-mockmodule-perl and liburi-db-perl.
    * sqitch: Drop versioned constraint on libhash-merge-perl,
      libio-pager-perl and liburi-db-perl in Depends.

  [ gregor herrmann ]
  * Drop uversionmangle from debian/watch.
    Upstream has changed to SemVer in their releases.
  * Import upstream version 1.2.1.
  * Update years of upstream and packaging copyright, and upstream
    copyright holders.
  * Declare compliance with Debian Policy 4.6.0.
  * Update (build) dependencies.
  * Override dh_strip_nondeterminism: fix permissions before.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Dec 2021 16:56:03 +0100

sqitch (1.1.0000-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.1.0000.
  * Add new build dependency: libtest-mockobject-perl.
  * Remove LC_ALL=C from build tests and autopkgtests.
    Tests are fixed to work under non-English locales.
  * Drop setting SQITCH_ORIG_FULLNAME for auopkgtests.
    Tests are fixed to work in environments where the user has no name set.
  * Update years of upstream copyright.
  * Bump debhelper-compat to 13.
  * debian/rules: drop handling of $HOME.
    Done by debhelper 13 for us.

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Jul 2020 17:29:00 +0200

sqitch (1.0.0000-3) unstable; urgency=medium

  * Team upload.
  * Add build and runtime dependencies on libpod-parser-perl.

 -- Niko Tyni <ntyni@debian.org>  Mon, 18 May 2020 21:58:26 +0300

sqitch (1.0.0000-2) unstable; urgency=medium

  * Team upload.
  * Set SQITCH_ORIG_FULLNAME in debian/tests/pkg-perl/smoke-env.
    Apparently the user which runs the autopkgtests on ci.debian.net has
    no name set, so the test if the name is set from the system fails.

 -- gregor herrmann <gregoa@debian.org>  Fri, 28 Feb 2020 14:27:31 +0100

sqitch (1.0.0000-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/*: replace ADTTMP with AUTOPKGTEST_TMP.

  [ Debian Janitor ]
  * Update standards version, no changes needed.
  * Bump debhelper from old 11 to 12.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 1.0.0000
  * Update (build) dependencies.
  * Update short description.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

 -- gregor herrmann <gregoa@debian.org>  Sun, 23 Feb 2020 11:56:51 +0100

sqitch (0.9999-2) unstable; urgency=medium

  * Team upload.
  * Add back libio-pager-perl to Depends.
    Thanks to Tommi Vainikainen for the bug report. (Closes: #922436)

 -- gregor herrmann <gregoa@debian.org>  Fri, 15 Feb 2019 22:57:27 +0100

sqitch (0.9999-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.9999.
  * Refresh fix-bad-whatis-man.patch (fuzz).
  * Update years of upstream and packaging copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.
  * Add debian/NEWS with a warning about deprecations, removals, and
    API changes.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Feb 2019 18:48:17 +0100

sqitch (0.9998-2) unstable; urgency=medium

  * Team upload.
  * debian/control: add version constraint to liburi-db-perl (build)
    dependency.
    Thanks to Tommi Vainikainen for the bug report. (Closes: #911576)

 -- gregor herrmann <gregoa@debian.org>  Mon, 22 Oct 2018 16:33:36 +0200

sqitch (0.9998-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.9998.
  * Update debian/upstream/metadata.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Mon, 08 Oct 2018 21:50:09 +0200

sqitch (0.9997-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.9997.
  * Update years of upstream copyright and add additional copyright
    holders.
  * Declare compliance with Debian Policy 4.1.5.
  * Use HTTPS for Homepage field in debian/control.
    Thanks to duck.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jul 2018 20:31:30 +0200

sqitch (0.9996-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.9996.
  * Update years of upstream and packaging copyright.
  * New build dependency: libmodule-runtime-perl.
  * Declare compliance with Debian Policy 4.1.3.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jan 2018 21:25:08 +0100

sqitch (0.9995-2) unstable; urgency=medium

  * Team upload.
  * Update MariaDB/MySQL alternative dependencies. (Closes: #848463)
  * Update years of packaging copyright.
  * Update versioned (build) dependencies.
    Drop unnecessary version constraints.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Dec 2016 18:01:55 +0100

sqitch (0.9995-1) unstable; urgency=medium

  * New upstream version, drop patch for DateTime.pm.

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 30 Jul 2016 21:18:25 +0000

sqitch (0.9994-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Christian Hofstaedtler ]
  * Update Standards-Version to 3.9.8
  * New upstream version
  * Import patch from upstream replacing DateTime::set(locale=>)
    with DateTime::set_locale() to fix warnings and subsequent
    failure in testsuite.

 -- Christian Hofstaedtler <zeha@debian.org>  Mon, 04 Jul 2016 14:12:17 +0200

sqitch (0.9993-2) unstable; urgency=medium

  * Team upload.
  * Set LC_ALL=C for the test suite, it breaks in other locales.
    (Closes: #800070)
  * Make the package autopkgtestable.

 -- Niko Tyni <ntyni@debian.org>  Sat, 26 Sep 2015 20:59:45 +0300

sqitch (0.9993-1) unstable; urgency=medium

  * Import upstream version 0.9993

 -- Christian Hofstaedtler <zeha@debian.org>  Fri, 21 Aug 2015 21:15:22 +0000

sqitch (0.9992-1) unstable; urgency=medium

  * New upstream version.
  * Update copyright notice for Debian packaging.

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 14 Jun 2015 12:59:52 +0200

sqitch (0.9991-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 0.9991
    Fixes "FTBFS: new warnings" (Closes: #785229)
  * debian/watch: add uversionmangle in case upstream goes back from 4
    minor digits to less.
  * Update fix-bad-whatis-man.patch.
  * Move libmodule-build-perl to Build-Depends (needed during clean).
  * Make (build) dependency on libpath-class-perl versioned.
  * Drop a couple of version constraint from (build) dependencies.
    They are all satisfied in (old)oldstable.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Thu, 14 May 2015 22:28:40 +0200

sqitch (0.996-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.

  [ Christian Hofstaedtler ]
  * New upstream version.
  * Remove upstream-supplied and -applied Digest::SHA patch.

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 28 Sep 2014 16:59:27 +0200

sqitch (0.995-1) unstable; urgency=low

  * Initial Release. (Closes: #751740)
  * Add patch from upstream to use Digest::SHA instead of Digest::SHA1.
  * Many thanks to gregor herrmann <gregoa@debian.org> for review and
    packaging suggestions.

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 24 Jul 2014 23:59:59 +0200
